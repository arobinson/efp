#!/usr/bin/env python

## AR: add this so it can load a virtualenv for its python
activate_this = "/home/arobinson/venv/py2.7efp/bin/activate_this.py"
execfile(activate_this, dict(__file__=activate_this))
## end addition

import os
import cgi
import tempfile
import string
import efp, efpXML, efpConfig, efpService, efpBase
import re


form = cgi.FieldStorage(keep_blank_values=1)



# Retrieve cgi inputs
VARS = {}
for var in ("dataSource", "primaryGene", "secondaryGene", 
            "threshold", "orthoListOn", "ncbi_gi", 
            "threshold", "modeInput", "override", 
            "modeMask_low", "modeMask_stddev"):
    VARS[var] = form.getvalue(var)

# dataSource    = form.getvalue("dataSource")
# primaryGene   = form.getvalue("primaryGene")
# secondaryGene = form.getvalue("secondaryGene")
# orthoListOn   = form.getvalue("orthoListOn")
# ncbi_gi       = form.getvalue("ncbi_gi")
# 
# thresholdIn   = form.getvalue("threshold")
# mode          = form.getvalue("modeInput")
# useThreshold  = form.getvalue("override")
# grey_low      = form.getvalue("modeMask_low")
# grey_stddev   = form.getvalue("modeMask_stddev")
testing_efp = open(efpConfig.OUTPUT_FILES+"/testing_efpweb.txt", "w")

# create a dictionary from efpConfig module
CONF = {}
for k in dir(efpConfig):
    if not k.startswith('_'):
        CONF[k] = getattr(efpConfig, k)

CONF['webservice_gene1'] = None
CONF['webservice_gene2'] = None

## process request
result = efpBase.processRequest(VARS, CONF)


## put variables back in scope for rendering code (not pretty)
_g = globals()
for k,v in result.items():
    _g[k] = v


###-------------------------------------------------------HTML codes----------------------------------------------------------------------------------------------------------------------
print 'Content-Type: text/html\n'
print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">'
print '<html>'
print '<head>'
print '  <title>%s eFP Browser</title>' % efpConfig.spec_names[efpConfig.species]
print '  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'
print '  <meta name="keywords" content="%s, genomics, expression profiling, mRNA-seq, Affymetrix, microarray, protein-protein interactions, protein structure, polymorphism, subcellular localization, proteomics, poplar, rice, Medicago, barley, transcriptomics, proteomics, bioinformatics, data analysis, data visualization, AtGenExpress, PopGenExpress, cis-element prediction, coexpression analysis, Venn selection, molecular biology">' % efpConfig.spec_names[efpConfig.species]
print '  <link rel="stylesheet" type="text/css" href="%s/efp.css"/>' % efpConfig.STATIC_FILES_WEB
print '  <link rel="stylesheet" type="text/css" href="%s/domcollapse.css"/>' % efpConfig.STATIC_FILES_WEB
print '  <script type="text/javascript" src="%s/efp.js"></script>' % efpConfig.STATIC_FILES_WEB
print '  <script type="text/javascript">'
print '    regId = /%s/i;' % efpConfig.inputRegEx
print '  </script>'
print '  <script type="text/javascript" src="%s/domcollapse.js"></script>' % efpConfig.STATIC_FILES_WEB
print '</head>'

print '<body><form action="efpWeb.cgi" name="efpForm" method="POST" onSubmit="return checkAGIs(this)">'
print '<table width="788" border="0" align="center" cellspacing="1" cellpadding="0">'
print '<tr><td>'
print '  <span style="float:right; top:0px; left:538px; width:250px; height:75px;">'
#print '    <script type="text/javascript"><!--'
#print '      google_ad_client = "pub-4138435367173950";'
#print '      /* BAR 234x60, created 20-Nov-2009 */'
#print '      google_ad_slot = "5308841066";'
#print '      google_ad_width = 234;'
#print '      google_ad_height = 60;'
#print '    //-->'
#print '    </script>'
#print '    <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>'
print '    <script type="text/javascript" src="%s/popup.js"></script>' % efpConfig.STATIC_FILES_WEB
print '  </span>'
print "<h1 style='vertical-align:middle;'><a href='http://142.150.215.220'><img src='http://142.150.215.220/bbc_logo_small.gif' alt='To the Bio-Array Resource Homepage' border=0 align=absmiddle></a>&nbsp;<img src='http://142.150.215.220/bar_logo.gif' alt='The Bio-Array Resource' border=0 align=absmiddle>&nbsp;<img src='http://142.150.215.220/images/eFP_logo_large.png' align=absmiddle border=0>&nbsp;%s eFP Browser<br><img src='http://142.150.215.220/images/green_line.gif' width=98%% alt='' height='6px' border=0></h1>" % efpConfig.spec_names[efpConfig.species]

print '</td></tr>'
print '<tr><td align="middle">'
print '    <table>'
print '      <tr align = "center"><th>Data Source</th>'
print '      <th>Mode'
print '<input type="checkbox" name="modeMask_stddev" title="In Absolute Mode, check to mask samples that exhibit a standard deviation of more than 50 percent of the signal value" '
if grey_stddev == "on":
    print 'checked'
print ' value="on" />'
print '<input type="checkbox" name="modeMask_low" title="In Relative Mode, check to mask the use of low expression values in ratio calculations" '
if grey_low == "on":
    print 'checked'
print ' value="on" />'
print '</th><th><a href="http://ricedb.plantenergy.uwa.edu.au/" target=_new title="Convert IDs to LOC_Os identifiers using the RiceDB tool at the University of Western Australia">Primary Gene ID</a></th><th><a href="http://ricedb.plantenergy.uwa.edu.au/" target=_new title="Convert IDs to LOC_Os identifiers using the RiceDB tool at the University of Western Australia">Secondary Gene ID</a></th>'
print '      <th id="t1">Signal Threshold<input type="checkbox" name="override" title="Check to enable threshold" onclick="checkboxClicked(this.form)" '
if useThreshold == "on":
    print 'checked'
print       ' value="on" />'
print '</th><th></th></tr>'
print '      <tr><td>'

# Help Link
print '      <img src="http://142.150.215.220/affydb/help.gif" border=0 align="top" alt="Click here for instructions in a new window" onClick="HelpWin = window.open(\'http://142.150.215.220/affydb/BAR_instructions.html#efp\', \'HelpWindow\', \'width=600,height=300,scrollbars,resizable=yes\'); HelpWin.focus();">&nbsp;'

# Build drop down list of Data Sources
if mode == None:
    print '<select name="dataSource" onchange="location.href=\'efpWeb.cgi?dataSource=\' + this.options[this.selectedIndex].value ;">'
elif useThreshold == None:
    thresholdSwitch = ""
    print '      <select name="dataSource" onchange="location.href=\'efpWeb.cgi?dataSource=\' + this.options[this.selectedIndex].value + \'&modeInput=%s&primaryGene=%s&secondaryGene=%s&override=%s&threshold=%s&modeMask_low=%s&modeMask_stddev=%s\' ;">' %(mode, primaryGene, secondaryGene, thresholdSwitch, threshold, grey_low, grey_stddev)
else:
    print '      <select name="dataSource" onchange="location.href=\'efpWeb.cgi?dataSource=\' + this.options[this.selectedIndex].value + \'&modeInput=%s&primaryGene=%s&secondaryGene=%s&override=%s&threshold=%s&modeMask_low=%s&modeMask_stddev=%s\' ;">' %(mode, primaryGene, secondaryGene, useThreshold, threshold, grey_low, grey_stddev)

xML = efpXML.findXML(efpConfig.dataDir)
for x in sorted(xML):
    print '    <option value="%s"' % x
    # To preserve modes between form submits
    if dataSource == x:
        print 'selected'
    xText = string.replace(x,'_',' ')
    print '>%s</option>' % xText
print '      </select></td>'
# xML = None
# reload(efpXML)

# Build drop down list of modes
if mode == None:
	print '      <td><select selected="Absolute" name="modeInput" onchange="changeMode(this.form)">' 
else:
	print '		 <td><select selected="Absolute" name="modeInput" onchange="location.href=\'efpWeb.cgi?dataSource=%s&modeInput=\' + this.options[this.selectedIndex].text + \'&primaryGene=%s&secondaryGene=%s&modeMask_low=%s&modeMask_stddev=%s\' ">' %(dataSource, primaryGene, secondaryGene, grey_low, grey_stddev)

# Preserve mode between form submits. If the user selected 'Compare' as his/her
# mode, when the page reloads, the list should still have 'Compare' selected.
if mode == 'Relative':
    print '    <option>Absolute</option>'
    print '    <option selected>Relative</option>'
    if dataSource != "rice_leaf_gradient" and dataSource != "rice_maize_comparison":
        print '    <option>Compare</option>'
elif mode == 'Compare':
    print '    <option>Absolute</option>'
    print '    <option>Relative</option>'
    print '    <option selected>Compare</option>'
else: # Default (Absolute)
    print '    <option selected>Absolute</option>'
    print '    <option>Relative</option>'
    if dataSource != "rice_leaf_gradient" and dataSource != "rice_maize_comparison":
        #testing_efp.write("WENT IN HERE")
        print '    <option>Compare</option>'

print '      </select></td><td>'
print '      <input type="text" id="g1" name="primaryGene" value="%s" size=17/></td><td>' % primaryGene
print '      <input type="text" id="g2" name="secondaryGene" size=17 value="%s" ' % secondaryGene
if mode != 'Compare' or dataSource == "rice_leaf_gradient" and dataSource == "rice_maize_comparison":
    #if mode != 'Compare':
    print 'disabled'
print '      /></td><td>'

print '      <input type="text" id="t0" name="threshold" value="%s" ' % threshold
if useThreshold == None: 
    print 'disabled'
print '      /></td>'
print '      <td><input type="submit" value="Go"/></td></tr>'
print '    </table>'
print '    </form>'
print '</td></tr>'
print '<tr><td>'

if error:
    print '    <ul>'
    for row in errorStrings:
        print '<li class="error">%s</li>' % row
    print '    </ul>'

###----------------------print orthologs-------------------------------------------------------------------------------------------------------------------------------------------------
for spec in orthologStrings:
    if len(orthologStrings[spec]) > 0:
    	print '<p class="expanded"> Links to %s Orthologs in %s eFP Browser </p>' % (efpConfig.spec_names[spec], efpConfig.spec_names[spec])
    
        print '<div>'
        print '    <ul>'
    
    	print '<p>Orthologs have been ranked based on Spearman Correlation Coefficient (SCC) value. Values are given for each ortholog.Note that these rankings are to be used at the users discretion, as expression profiles were compared using limited data points.</p>'
    
        for row in orthologStrings[spec]:
            print '<li>%s</li>'%row
        print '    </ul>'
        print '</div>'

if len(testalertStrings) > 0:
    print '    <ul>'
    for row in testalertStrings:
        print '<li>%s</li>' % row
    print '    </ul>'

# print additional header text if configured for selected data source
if dataSource in efpConfig.datasourceHeader:
    print '%s' % efpConfig.datasourceHeader[dataSource]
elif 'default' in efpConfig.datasourceHeader:
    print '%s' % efpConfig.datasourceHeader['default']

if len(alertStrings) > 0:
    print '    <ul>'
    for row in alertStrings:
        print '<li>%s</li>' % row
    print '    </ul>'

if mode != None and error == 0:
    ###----------------------check external services-------------------------------------------------------------------------------------------------------------------------------------------------
    # Serialize services data from XML file into a Info object
    info = efpService.Info()
    if (info.load("%s/efp_info.xml" % efpConfig.dataDir) == None):
        print '<table style="margin-left:auto; margin-right:0;"><tr>'
        testing_efp.write("primaryGene after alterGene = %s\n"%webservice_gene1.webservice_gene)        
        for name in (info.getServices()):
            service = info.getService(name)
            external = service.getExternal()
            highlight1 = service.checkService(webservice_gene1.webservice_gene)
            testing_efp.write("HIGHLIGHT1 = %s\n"%highlight1)
            highlight2 = None
            if(mode == 'Compare'):
                highlight2 = service.checkService(webservice_gene2.webservice_gene)
            if highlight1 == 'error' or highlight2 == 'error':
                print '<td><img title="connection error for service %s" width="50" height="50" alt="connection error" src="%s/error.png"></td>'% (name, efpConfig.dataDir)
                continue
            elif (highlight1):
                link = service.getLink(webservice_gene1.webservice_gene)
                gene = webservice_gene1.webservice_gene
            elif (highlight2):
                link = service.getLink(webservice_gene2.webservice_gene)
                gene = webservice_gene1.webservice_gene
            else:
                print '<td><img title="No %s data found" width="50" height="50" alt="No %s data found" style="opacity:0.30;filter:alpha(opacity=30);" src="%s/%s"></td>'%(name, name, efpConfig.dataDir, service.icon)
                continue
            if link:
                if external == "true":
                    print '<td><a target="_blank" title="%s gene %s" href="%s"><img width="50" height="50" alt="%s gene %s" src="%s/%s"></a></td>'%(name, gene, link, name, gene, efpConfig.dataDir, service.icon)
                else:
                    print '<td><a target="_blank" title="%s for gene %s" href="%s"><img width="50" height="50" alt="%s for gene %s" src="%s/%s"></a></td>'%(name, gene, link, name, gene, efpConfig.dataDir, service.icon)
            else:
                print '<td><img target="_blank" title="%s found for gene %s" width="50" height="50" alt="%s found for %s" src="%s/%s"></td>'%(name, gene, name, gene, efpConfig.dataDir, service.icon)
        print '</tr></table>'

###----------------------print the image-------------------------------------------------------------------------------------------------------------------------------------------------
    view_no = 1
    for view_name in views:
        print '<tr><td>'
        imgFile = imgFilename[view_name];
        temp_imgPath = imgFilename[view_name].split("/")
        last_element = temp_imgPath[-1]
        match = re.search('^efp', last_element) 
        if match is not None:
            imgFile = efpConfig.OUTPUT_FILES+'/%s'%(last_element)
        print '  <img src="%s/%s" border="0" ' % (conf["OUTPUT_FILES_WEB"],os.path.basename(imgFile))
        if view_name in imgMap:
            print 'usemap="#imgmap_%s">'%view_name
            print '%s' % imgMap[view_name]
        else:
            print '>'
        print '</td></tr>'
        # Creates Button and Link to Page for Table of Expression Values
        print '<tr align="center"><td><br>'
        temp_tablePath = tableFile[view_name][1].split("/")
        tableFile_name = conf["OUTPUT_FILES_WEB"]+'/%s'%(temp_tablePath[-1])
    #    print '<input type="button" name="expressiontable" value="Click Here for Table of Expression Values" onclick="location.href=\'%s\'">' % tableFile_name
        print '<input type="button" name="expressiontable" value="Click Here for Table of Expression Values" onclick="resizeIframe(\'ifr%d\', ifr%d);popup(\'table%d\', \'fadein\', \'center\', 0, 1)">&nbsp;&nbsp;' % (view_no, view_no, view_no)
        tableChart_name = tableFile_name.replace(".html", ".png")
        print '<input type="button" name="expressionchart" value="Click Here for Chart of Expression Values" onclick="popup(\'chart%d\', \'fadein\', \'center\', 0, 1)">' % (view_no)
        print '<script type="text/javascript">'
        popup_content = '<span style="color:#000000"><b>For table download right click <a href="%s">here</a> and select "Save Link As ..."</b></span>' % tableFile_name
        popup_content += '<div class="closewin_text">'
        popup_content += '<a href="" onclick="popdown(\\\'table%d\\\');return false;">' % (view_no)
        popup_content += '<span style="color:#000000">[Close]</span></a><br><br>'
        popup_content += '<a href="" onclick="switchPopup(\\\'table%d\\\', \\\'chart%d\\\');return false;">' % (view_no, view_no)
        popup_content += '<span style="color:#000000">[Switch to<br> Chart]</span></a></div>'
        popup_content += '<div class="chart"><iframe id="ifr%d" name="ifr%d" width=900 frameborder=0 src="%s">' % (view_no, view_no, tableFile_name)
        popup_content += 'Your browser doesn\\\'t support iframes. Please use link above to open expression table</iframe></div>'
        popup_width = '1000';
        bg_color = '#FFFFFF';
        print "loadPopup(\'table%d\',\'%s\',\'%s\',%s);" % (view_no, popup_content, bg_color, popup_width)
        popup_content = '<div class="closewin_text">'
        popup_content += '<a href="" onclick="popdown(\\\'chart%d\\\');return false;">' % (view_no)
        popup_content += '<span style="color:#000000">[Close]</span></a><br><br>'
        popup_content += '<a href="" onclick="resizeIframe(\\\'ifr%d\\\', ifr%d);switchPopup(\\\'chart%d\\\', \\\'table%d\\\');return false;">' % (view_no, view_no, view_no, view_no)
        popup_content += '<span style="color:#000000">[Switch to<br>Table]</span></a><br><br>'
        popup_content += '<a href="" onclick="zoomElement(\\\'image%d\\\', 0.1);return false;">' %(view_no)
        popup_content += '<span style="color:#000000">[Zoom +]</span></a><br>'
        popup_content += '<a href="" onclick="zoomElement(\\\'image%d\\\', -0.1);return false;">' %(view_no)
        popup_content += '<span style="color:#000000">[Zoom -]</span></a><br>'
        popup_content += '<a href="" onclick="zoomElement(\\\'image%d\\\', 0);return false;">' % (view_no)
        popup_content += '<span style="color:#000000">[Reset<br>zoom]</span></a></div>'
        popup_content += '<div class="chart"><img id="image%d" height="580px" src="%s"><br></div>' % (view_no, tableChart_name)
        print "loadPopup(\'chart%d\',\'%s\',\'%s\',%s);" % (view_no, popup_content, bg_color, popup_width)
        print "</script>"
        print '<br></td></tr>'
        view_no = view_no + 1
    print '  <tr><td><br><ul>'
    print '  <li>%s was used as the probe set identifier for your primary gene, %s (%s)</li>' % (gene1.getProbeSetId(), gene1.getGeneId(), gene1.getAnnotation())
    if mode == 'Compare':
    	print '  <li>%s was used as the probe set identifier for the secondary gene, %s (%s)</li>' % (gene2.getProbeSetId(), gene2.getGeneId(), gene2.getAnnotation())
    print '  </ul>'
    if(dataSource in efpConfig.datasourceFooter):
        print efpConfig.datasourceFooter[dataSource]
    else:
        print efpConfig.datasourceFooter['default']
    print '</td></tr>'
    print '<tr><td><img src="http://142.150.215.220/bbclone/stats_image.php" title="" name="thumbnail" border="0" width="0px" height="0px"></td></tr>'
else:
    print '  <img src="%s" border="0">' % (defaultImgFilename)
print '</table>'
print '<input type=hidden name="orthoListOn" id="orthoListOn" value=\"0\">'
print '</body>'

print '</html>'
