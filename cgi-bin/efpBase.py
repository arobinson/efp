'''
Created on Jan 19, 2017

Code in this file was extracted from efpWeb.cgi to remove business logic
from the presentation logic.

@author: Andrew Robinson
@author: rtbreit
'''

import tempfile
import os
import re

import efp, efpXML, efpService
import logging

def processRequest(VARS, conf):
    '''
    Handles request for main web view and returns a dictionary with all values
    to be displayed
    
    @param VARS: dict object of get variables that were provided
    @param conf: dict object of configuration variables
    
    @return: dict object with values to place on website
    '''
    
    error = 0
    errorStrings = []
    alertStrings = []
    testalertStrings = []
    orthologStrings = {}
    alignOrthologs = {}
    lowAlert = 0
    sdAlert = 0
    
    img = None
    imgMap = {}
    imgFilename = {}
    tableFile = {}
    views = []
    
    webservice_gene1 = None
    webservice_gene2 = None
    
    dataSource    = VARS["dataSource"]
    primaryGene   = VARS["primaryGene"]
    secondaryGene = VARS["secondaryGene"]
    orthoListOn   = VARS["orthoListOn"]
    ncbi_gi       = VARS["ncbi_gi"]
    thresholdIn   = VARS["threshold"] or 0
    mode          = VARS["modeInput"]
    useThreshold  = VARS["override"]
    grey_low      = VARS["modeMask_low"]
    grey_stddev   = VARS["modeMask_stddev"]
    
    result = {}
    
    # Default gene id
    if primaryGene == None and ncbi_gi != None:
       primaryGene = efpDb.ncbiToAGI(ncbi_gi)
       if primaryGene == None:
          primaryGene == ncbi_gi
          errorStr = 'The requested NCBI gi "%s" doesn\'t correspond to a given AGI.<br>'%ncbi_gi
          errorStrings.append(errorStr)
    elif primaryGene == None:
       primaryGene = conf['GENE_ID_DEFAULT1']
    if secondaryGene == None:
        secondaryGene = conf['GENE_ID_DEFAULT2']
    
    if useThreshold == "":
        useThreshold = None
    
    # set orthoListOn to off
    orthoListOn = "0"
    
    # Try Entered Threshold; if fails or threshold not checked use default threshold
    if useThreshold != None:
        try:
            threshold = float(thresholdIn) # Convert str to float
        except:
            # Threshold string was malformed
            error = 1
            errorStr = 'Invalid Threshold Value "%s"<br>' % thresholdIn
            errorStrings.append(errorStr)
            useThreshold = None
    if useThreshold == None and thresholdIn == None:
        # assign a default value for first calls
        if mode == "Relative" or mode == "Compare":
            threshold = 2.0
        else:    #Absolute or none
            threshold = 500
        firstCall = 1
    else:
        threshold = float(thresholdIn)
        firstCall = 0
    
    if dataSource == None:
        dataSource = conf['defaultDataSource']
    
    # Serialize data from XML file into a Specimen object
    spec = efp.Specimen(conf)
    xmlName = "%s/%s.xml" % (conf['dataDir'], dataSource)
    spec.load(xmlName)
    
    # Right now the browser only has one view - "all"
    # In the future, there should be a drop down menu letting users
    # choose multiple views
    
    defaultImgFilename = "%s/%s.png" % (conf['dataDirWeb'], dataSource)
    if mode == None:
        # If no mode is selected (99% of the time this means the user just arrived
        # at the page), just show them a color map
        
        # Set Developmental_Map as default DataSource
        if dataSource == None:
            dataSource = conf['defaultDataSource']
    
    else:        
    ###-----------------------------------"all" view-------------------------------------------------------------------------------------------------------------------------------
        for name, view in spec.getViews().iteritems():
            # If either of these probe IDs are None (bad inputs), then we just
            # spit out the default image again
            gene1 = view.createGene(primaryGene)
            gene2 = view.createGene(secondaryGene)
            gene1 = view.alterGene(gene1)
            gene2 = view.alterGene(gene2)
            webservice_gene1 = view.alterWebserviceGene(gene1)
            webservice_gene2 = view.alterWebserviceGene(gene2)
#             testing_efp.write("Probeset for Gene2 = %s\n"%(gene2.getProbeSetId()))
            if gene1.getGeneId() == None:
            #if gene1.getProbeSetId() == None:
                errorStr = 'The requested Primary gene / probeset ID "%s" cannot be found in %s datasource ' % (primaryGene, view.dbGroup)
                error = 1
                errorStrings.append(errorStr)
            elif mode == 'Compare' and gene2.getGeneId() == None:
            
            #elif mode == 'Compare' and gene2.getProbeSetId() == None:
                error = 1
                errorStr = 'The requested Secondary gene / probeset ID "%s" cannot be found in %s datasource <br>' % (secondaryGene, view.dbGroup)
                errorStrings.append(errorStr)
            elif mode == 'Compare' and (dataSource == "rice_leaf_gradient" or dataSource == "rice_maize_comparison"):
                error = 1
                errorStr = 'Compare mode is disabled for this dataset <br>'
                errorStrings.append(errorStr)
            elif primaryGene == secondaryGene and mode == 'Compare':
                error = 1
                errorStr = 'The requested Secondary gene / probeset ID "%s" must be different than the Primary ID<br>' % secondaryGene
                errorStrings.append(errorStr)
                viewMaxSignal = 2.0
            else:
                if mode == 'Absolute':
                    if useThreshold:
                        (img,viewMaxSignal,viewMaxSignal1,viewMaxSignal2,sdAlert) = view.renderAbsolute(gene1, threshold, grey_mask=grey_stddev)
                    else:
                        (img,viewMaxSignal,viewMaxSignal1,viewMaxSignal2,sdAlert) = view.renderAbsolute(gene1, grey_mask=grey_stddev)
                elif mode == 'Relative':
                    if useThreshold:
                        (img,viewMaxSignal,viewMaxSignal1,viewMaxSignal2,lowAlert) = view.renderRelative(gene1, threshold, grey_mask=grey_low)
                    else:
                        (img,viewMaxSignal,viewMaxSignal1,viewMaxSignal2,lowAlert) = view.renderRelative(gene1, grey_mask=grey_low)
                elif mode == 'Compare':    
                    if useThreshold:
                        
                        (img,viewMaxSignal,viewMaxSignal1,viewMaxSignal2) = view.renderComparison(gene1, gene2, threshold)
                    else:
#                         testing_efp.write("WENT THROUGH HERE\n")
                        (img,viewMaxSignal,viewMaxSignal1,viewMaxSignal2) = view.renderComparison(gene1, gene2)
    
                # find the max signal across all datasources and provide a link to that datasource
                (maxSignalInDatasource, maxDatasource) = view.getMaxInDatasource(gene1)
                
                maxSignalInDatasource = round(maxSignalInDatasource,2)
                # maxDatasource = re.sub("_"," ", maxDatasource)
                alertStr = "For %s data, this probe set reaches its maximum expression level (expression potential) of <b>%s</b> in the <b>%s</b> data source." % (view.dbGroup, maxSignalInDatasource, maxDatasource)
                alertStrings.append(alertStr)
    
                # alert the user that the scale has changed if no threshold is set
                if useThreshold == None and firstCall != 1:
                    if viewMaxSignal > threshold:
                        useThresholdFlag = "on"
                        thresholdLevelSuggested = maxSignalInDatasource
                        if mode == 'Relative':
                            thresholdLevelSuggested = 4
                        if mode == 'Compare':
                            thresholdLevelSuggested = 4
                        alertStr = "For %s data, note the maximum signal value has increased to %s from %s. Use the <a href='efpWeb.cgi?dataSource=%s&modeInput=%s&primaryGene=%s&secondaryGene=%s&override=%s&threshold=%s&modeMask_low=%s&modeMask_stddev=%s'>Signal Threshold option to keep it constant at %s</a>, or enter a value in the Signal Threshold box, such as <a href='efpWeb.cgi?dataSource=%s&modeInput=%s&primaryGene=%s&secondaryGene=%s&override=%s&threshold=%s&modeMask_low=%s&modeMask_stddev=%s'>%s</a>. The same colour scheme will then be applied across all views.<br>" % (view.dbGroup, viewMaxSignal, threshold, dataSource, mode, primaryGene, secondaryGene, useThresholdFlag, threshold, grey_low, grey_stddev, threshold, dataSource, mode, primaryGene, secondaryGene, useThresholdFlag, thresholdLevelSuggested, grey_low, grey_stddev, thresholdLevelSuggested)
                        alertStrings.append(alertStr)
                    elif viewMaxSignal < threshold:
                        useThresholdFlag = "on"
                        thresholdLevelSuggested = maxSignalInDatasource
                        if mode == 'Relative':
                            thresholdLevelSuggested = 4
                        if mode == 'Compare':
                            thresholdLevelSuggested = 4
                        alertStr = "For %s data, note the maximum signal value has decreased to %s from %s. Use the <a href='efpWeb.cgi?dataSource=%s&modeInput=%s&primaryGene=%s&secondaryGene=%s&override=%s&threshold=%s&modeMask_low=%s&modeMask_stddev=%s'>Signal Threshold option to keep it constant at %s</a>, or enter a value in the Signal Threshold box, such as <a href='efpWeb.cgi?dataSource=%s&modeInput=%s&primaryGene=%s&secondaryGene=%s&override=%s&threshold=%s&modeMask_low=%s&modeMask_stddev=%s'>%s</a>. The same colour scheme will then be applied across all views.<br>" % (view.dbGroup, viewMaxSignal, threshold, dataSource, mode, primaryGene, secondaryGene, useThresholdFlag, threshold, grey_low, grey_stddev, threshold, dataSource, mode, primaryGene, secondaryGene, useThresholdFlag, thresholdLevelSuggested, grey_low, grey_stddev, thresholdLevelSuggested)
                        alertStrings.append(alertStr)
                    else:
                        alertStr = ""
                    threshold = viewMaxSignal
                elif useThreshold == None and firstCall == 1:
                    threshold = viewMaxSignal
    
                # alert the user if SD filter or low filter should be activated
                if grey_stddev != "on" and sdAlert == 1 and mode == 'Absolute':
                    grey_stddev_flag = "on"
                    if useThreshold == None:
                        useThreshold = ""
                    alertStr = "Some samples exhibit high standard deviations for replicates. You can use <a href='efpWeb.cgi?dataSource=%s&modeInput=%s&primaryGene=%s&secondaryGene=%s&override=%s&threshold=%s&modeMask_low=%s&modeMask_stddev=%s'>standard deviation filtering</a> to mask those with a deviation greater than half their expression value.<br>" % (dataSource, mode, primaryGene, secondaryGene, useThreshold, threshold, grey_low, grey_stddev_flag)
                    alertStrings.append(alertStr)
                # alert the user if SD filter or low filter should be activated
                if grey_low != "on" and lowAlert == 1 and mode == 'Relative':
                    grey_low_flag = "on"
                    if useThreshold == None:
                        useThreshold = ""
                    alertStr = "Some sample ratios were calculated with low values that exhibit higher variation, potentially leading to ratios that are not a good reflection of the biology. You can <a href='efpWeb.cgi?dataSource=%s&modeInput=%s&primaryGene=%s&secondaryGene=%s&override=%s&threshold=%s&modeMask_low=%s&modeMask_stddev=%s'>low filter below 20 units</a> to mask these.<br>" % (dataSource, mode, primaryGene, secondaryGene, useThreshold, threshold, grey_low_flag, grey_stddev)
                    alertStrings.append(alertStr)
    
                # Otherwise, we render and display the option
                imgMap[view.name] = view.getImageMap(mode, gene1, gene2, useThreshold, threshold, dataSource, grey_low, grey_stddev)
    
            if img != None:
                imgFilename[view.name] = view.drawImage(mode, maxSignalInDatasource, viewMaxSignal1, viewMaxSignal2, gene1, gene2, img)
                #Create a table of Expression Values and save it in a temporary file
                expTable = view.table
                tableFile[view.name] = tempfile.mkstemp(suffix='.html', prefix='efp-', dir=conf["OUTPUT_FILES"])
                os.system("chmod 644 " + tableFile[view.name][1])
                tf = open(tableFile[view.name][1], 'w')
                tf.write(expTable)
                tf.close()
                chartFile = tableFile[view.name][1].replace(".html", ".png")
                view.saveChart(chartFile, mode)
                views.append(view.name)
            
    ###------------------------------------------------------- Expresso logs ----------------------------------------------------------------------------------------------------------------------
    
    #if (error == 0 and mode != None and conf['species'] in conf['ortholog_species']):
    #    ## get orthologs for configured species if it exists
    #    for spec in conf['ortholog_species']:
    #        scc_genes, scc_orthologs, align_orthologs = gene1.getOrthologs(conf['species'], spec)
    #        orthologStrings[spec] = []
    #        if scc_orthologs == None:
    #            orthoStr = "There are no %s orthologs for %s"%(conf['spec_names'][spec], gene1.getGeneId())
    #            orthologStrings[spec].append(orthoStr)
    #        else:
    #            alignOrthologs.update(align_orthologs)
    #            for scc, probeset in sorted (scc_orthologs.iteritems(), reverse=True): 
    #                probeset1 = scc_orthologs[scc]
    #                
    #                for probeset in probeset1:
    #                    scc_gene1 = scc_genes.get(probeset, 0)
    #                    link = conf['efpLink'][spec] %  scc_gene1
    #                    orthoStr = "View <a href='%s'>expression pattern</a> for gene %s\'s %s ortholog %s (%s). SCC Value: %s" % \
    #                                (link, gene1.getGeneId(), conf['spec_names'][spec], scc_gene1, probeset, scc)
    #                    orthologStrings[spec].append(orthoStr)
    #    agi_seq = gene1.getSequence()
    #    if agi_seq is not None and len(alignOrthologs) > 0:
    #        fasta_file = efp.write_fasta(alignOrthologs, gene1.getGeneId(), agi_seq)
    #        alignment_file, new_outfile = efp.mafft_align(fasta_file)
    #        
    #        os.unlink(fasta_file)
    #        alertStr = "View <a href='%s'>alignment</a> for any orthologs for %s. <a href='%s'>Download alignment</a>."%(alignment_file, primaryGene, new_outfile)
    #    species_name = conf['spec_names'][conf['species']]
    #    alertStr = "View <a href='../../expressolog_treeviewer/cgi-bin/expressolog_treeviewer.cgi?primaryGene=%s&species=%s&dataset=%s&checkedspecies=%s'>tree</a> with sequence and expression similarity, and alignment, for all orthologs for %s."%(gene1.getGeneId(),species_name,"Developmental",conf['checkedspecies'],gene1.getGeneId())
    #    testalertStrings.append(alertStr)
    
    
    if mode != None and error == 0:
        
        ## process links ##
        info = efpService.Info()
        if (info.load("%s/efp_info.xml" % conf["dataDir"]) == None):
            services = []
            for name in (info.getServices()):
                s = {}
                gene = None
                link = None
                s['name'] = name
                service = info.getService(name)
                s['service'] = service
                external = service.getExternal()
                highlight1 = service.checkService(webservice_gene1.webservice_gene)
                highlight2 = None
                if(mode == 'Compare'):
                    highlight2 = service.checkService(webservice_gene2.webservice_gene)
                if highlight1 == 'error' or highlight2 == 'error':
                    continue
                elif (highlight1):
                    link = service.getLink(webservice_gene1.webservice_gene)
                    gene = webservice_gene1.webservice_gene
                elif (highlight2):
                    link = service.getLink(webservice_gene2.webservice_gene)
                    gene = webservice_gene1.webservice_gene
                else:
                    continue
                s['gene'] = gene
                s['link'] = link
                services.append(s)
        
        ## process views ##
        view_no = 1
        views_proc=[]
        for view_name in views:
            view = {
                    'view_no': view_no,
                    'view_name': view_name,
                   }
            view['imgFile'] = imgFilename[view_name]
            view['imgFileWeb'] = "%s/%s" % (conf["OUTPUT_FILES_WEB"],os.path.basename(view['imgFile']))
            if view_name in imgMap:
                view['imgMap'] = imgMap[view_name]
            view['last_element'] = os.path.basename(imgFilename[view_name])
            match = re.search('^efp', view['last_element'])
            if match is not None:
                view['imgFile'] = conf["OUTPUT_FILES_WEB"]+'/%s'%(view['last_element'])
            view['tableFile_name'] = conf["OUTPUT_FILES_WEB"]+'/%s'%(os.path.basename(tableFile[view_name][1]))
            view['tableChart_name'] = view['tableFile_name'].replace(".html", ".png")
            view['popup_width'] = '1000'
            view['bg_color'] = '#FFFFFF'
            
            views_proc.append(view)
            
            view_no += 1
    
    ## make list of dataSources ##
    datasources = []
    xML = efpXML.findXML(conf["dataDir"])
#     logging.error(xML)
    for x in sorted(xML):
        src = (x, x.replace('_',' '))
        datasources.append(src)
    
    ## keep all variables ##
    for k, v in globals().items():
        if not k.startswith('_'):
            result[k] = v
    for k, v in locals().items():
        if not k.startswith('_'):
            result[k] = v
    
    return result

