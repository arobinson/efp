# eFP Browser

This is a fork of the original [eFP Browser](https://sourceforge.net/projects/efpbrowser/files/?source=navbar)

## Major changes

* Separated rendering from business logic
* Restructured the directory layout so each main group of files are separate (and move-able via settings)
	* code (cgi-bin/)
	* static files (static/)
	* data files (data/)
	* generated files (output/) 
* Allowed config to be supplied programmatically (allowing one code-base to be used by many separate data-sets)
